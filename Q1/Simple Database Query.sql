SELECT Child.ID, Child.UserName, Parent.UserName AS `ParentUserName`
FROM `USER` AS `Child`
LEFT JOIN `USER` AS `Parent` ON Parent.ID = Child.Parent;