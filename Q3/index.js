let text = "((ddd((dd)))aaa(a";
console.log(`Result from code: ${findFirstStringInBracket(text)}`)

function findFirstStringInBracket(str) {
    let indexFirstBracketFound = str.indexOf("(");
    let lastFirstBracketFound = str.indexOf(")");
    
    if(indexFirstBracketFound >= 0 && lastFirstBracketFound >= 0){
      // cara pertama menggunakan substr
      let wordsAfterFirstBracket = str.substr(indexFirstBracketFound + 1, (lastFirstBracketFound - indexFirstBracketFound) - 1);

      // cara pertama menggunakan substring
      // let wordsAfterFirstBracket = str.substring(indexFirstBracketFound + 1, lastFirstBracketFound);
      return wordsAfterFirstBracket;
    }
    else
    {
      return "";
    }
}