# Bibit Basic Test Code - AIZUL FAIZ ISWAFAZA

This repository is a task given by a company in Indonesia as a condition for selection, there are 4 challenges.

## Usage Directory Q1
contains an sql file as the answer to challenge number one "Simple Query"

## Usage Directory Q2
contains a simple fire project as an answer to challenge number two accessed (https://aizul-for-bibit-task.herokuapp.com/)

before running program, please to import the sql file first
```bash
cd Q2
npm install #for install library
npm run start #for running system
npm test #for running unit test
```

There are 3 endpoints with the GET method
- detail (https://aizul-for-bibit-task.herokuapp.com/api/v1/movies/detail)
- search (https://aizul-for-bibit-task.herokuapp.com/api/v1/movies/search)
- logs (https://aizul-for-bibit-task.herokuapp.com/api/v1/logs)

## Usage Directory Q3
contains the js file as the answer to challenge number three "FirstStringOnBracket"
make sure you have installed node js
```bash
cd Q3
node index.js
```

## Usage Directory Q4
contains the js file as the answer to challenge number four "Anagram"
make sure you have installed node js
```bash
cd Q4
node index.js
```