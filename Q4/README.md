```bash
var words = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua'];
var wordsNotDeleted = words;
var result = [];

for (var i = 0 ; i < words.length; i++) {
  var wordTemp = words[i].split("").sort().join("");
  var resultTemp = [];
  var remainingWords = [];

  for (var j = 0; j < wordsNotDeleted.length; j++) {
    var item = wordsNotDeleted[j].split("").sort().join("");
    
    if(wordTemp === item){
      resultTemp.push(wordsNotDeleted[j]);
    }else{
      remainingWords.push(wordsNotDeleted[j]);
    }
  }
  wordsNotDeleted = remainingWords;

  if(resultTemp.length > 0){
    result[result.length] = resultTemp;
  }
}

console.log(result);
```