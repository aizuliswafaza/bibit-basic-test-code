const logger = require('../services/loggerService')

class LogMiddleware {
    static set(req, res, next){
        logger.Logged(req);
        next()
    }
}

module.exports = LogMiddleware