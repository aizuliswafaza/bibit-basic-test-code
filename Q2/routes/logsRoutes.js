var express = require('express');
var router = express.Router();
const loggerService = require("../services/loggerService");

/* GET home page. */
router.get('/', loggerService.Datatable);
router.post('/', loggerService.Create);

module.exports = router;