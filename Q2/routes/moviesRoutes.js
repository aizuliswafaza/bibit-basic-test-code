var express = require('express');
var router = express.Router();
const movieService = require("../services/moviesService");

router.get('/detail', movieService.detail);
router.get('/detail/:id', movieService.findById);
router.get('/search', movieService.search);

module.exports = router;
