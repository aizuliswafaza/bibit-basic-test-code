var assert = require('assert');
var moviesServices = require('../services/moviesService');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('#search()', () => {
    it('Should be success when search title batman', (done) => {
      chai.request(server)
          .get('/api/v1/movies/search?s=batman')
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('Search');
              res.body.should.have.property('Response').eql("True");
              done();
          });
    });
});

describe('#detail()', () => {
    it('Should be success when find movieId tt0372784', (done) => {
      chai.request(server)
          .get('/api/v1/movies/detail?i=tt0372784&plot=full')
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('Title');
              res.body.should.have.property('imdbID').eql("tt0372784");
              res.body.should.have.property('Response').eql("True");
              done();
          });
    });
});

// describe('MovieServices', () => {
    

    
// });