class BaseResponse{
    constructor(res){
        this.status = 200;
        this.res = res;
        this.data = null
        this.message = null
    }

    setData(data){
        this.data = data
    }

    setStatus(status){
        this.status = status
    }

    setMessage(message){
        this.message = message
    }

    build(){
        return this.res.status(this.status).send({Data: this.data});
    }

    buildError(){
        return this.res.status(this.status).send({Message: this.message});
    }
}

module.exports = BaseResponse;