const BaseResponse = require("../responses/BaseResponse");
const SearchQueryFilter = require("../queryFilter/SearchQueryFilter");
const DetailQueryFilter = require("../queryFilter/DetailQueryFilter");
const qs = require('querystring')

exports.success = (req, res) => {
    var baseResponse = new BaseResponse(res);
    var qf = new SearchQueryFilter(req.query);

    baseResponse.setData(qs.stringify(qf));
    return baseResponse.build();
}

exports.failed = (req, res) => {
    var baseResponse = new BaseResponse(res);
    var qf = new DetailQueryFilter(req.query);

    baseResponse.setData(qs.stringify(qf));
    baseResponse.setStatus(500);
    return baseResponse.build();
}