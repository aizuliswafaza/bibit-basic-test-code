const { BadRequestCode, SuccessRequestCode } =  require(`../utils/constants`);

exports.index = (req, res) => {
    return res.status(SuccessRequestCode).json({Message: "Welcome to bibit_task"})
}