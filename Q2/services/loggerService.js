const { Sequelize, sequelize } = require('../models');
const { MstLogger } = require('../models/init-models').initModels(sequelize)
const { BadRequestCode, SuccessRequestCode } =  require(`../utils/constants`);
const moment = require('moment');
const qs = require('querystring')

exports.Datatable = async (req, res) => {
    let records = await MstLogger.findAndCountAll();
    return res.status(SuccessRequestCode).json(records);
}

exports.Create = async (req, res) => {
    let log = await MstLogger.create({
        executedAt: new Date(),
        method: req.method,
        endpoint: req.baseUrl + req.path,
        queryString: `?${qs.stringify(req.query)}`,
        payload: JSON.stringify(req.body)
    });
    
    return res.status(SuccessRequestCode).json(log);
}

exports.Logged = (req) => {
    MstLogger.create({
        executedAt: new Date(),
        method: req.method,
        endpoint: req.baseUrl + req.path,
        queryString: `?${qs.stringify(req.query)}`,
        payload: JSON.stringify(req.body)
    });
}