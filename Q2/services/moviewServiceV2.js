const { omdbEndpoint } =  require(`${__dirname}/../config.json`);
const { BadRequestCode, SuccessRequestCode } =  require(`../utils/constants`);
const { Logged } = require("./loggerService");
const https = require('request');
const qs = require('querystring')

exports.search = (req, res) => {
    getExternalApi(qs.stringify(req.query))
    .then((data) => {
        return res.status(SuccessRequestCode).json(data)
    }).catch((err) => {
        return res.status(BadRequestCode).json(err)
    });
}

exports.detail = async (req, res) => {
    let {i, plot} = req.query;

    if(!i){
        return res.status(BadRequestCode).json({Resposne: "False", Message: "Param Id is Required!"});
    }

    if(plot && !(["short", "full"]).includes(plot)){
        return res.status(BadRequestCode).json({Resposne: "False", Message: "sorry we only accept short and full plot"});
    }

    getExternalApi(qs.stringify(req.query))
    .then((data) => {
        return res.status(SuccessRequestCode).json(data)
    }).catch((err) => {
        return res.status(BadRequestCode).json(err)
    });
}

exports.findById = async (req, res) => {
    let id = req.params.id;
    let { plot } = req.query;

    if(plot && !(["short", "full"]).includes(plot)){
        return res.status(BadRequestCode).json({Resposne: "False", Message: "sorry we only accept short and full plot"});
    }

    getExternalApi(`i=${id}&${qs.stringify(req.query)}`)
    .then((data) => {
        return res.status(SuccessRequestCode).json(data)
    }).catch((err) => {
        return res.status(BadRequestCode).json(err)
    });
}

function getExternalApi (query){
    console.log("start to get value from omdb api")
    return new Promise((resolve, reject) => {
        https.get({
            url: `${omdbEndpoint}&${query}`,
        }, (err, res, body) => {
            var jsonBody = JSON.parse(body);
            if(jsonBody.Response === "True"){
                resolve(jsonBody);
            }
            else{
                reject(jsonBody);
            }
        }).on("error", (err) => {
            reject(err);
        })
    })
}