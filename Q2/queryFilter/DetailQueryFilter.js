var BaseQueryFilter = require('./BaseQueryFilter');

class DetailQueryFilter extends BaseQueryFilter{

    constructor(body){
        super(body);
        this.i = body.i == undefined ? null: body.i;
        this.t = body.t == undefined ? null: body.t;
        this.plot = body.plot == undefined ? "short": body.plot;
    }

}

module.exports = DetailQueryFilter;