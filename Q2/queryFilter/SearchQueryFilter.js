var BaseQueryFilter = require('./BaseQueryFilter');

class SearchQueryFilter extends BaseQueryFilter{

    constructor(body){
        super(body);
        this.s = body.s == undefined ? null: body.s;
        this.page = body.page == undefined ? 1: body.page;
    }

}

module.exports = SearchQueryFilter;