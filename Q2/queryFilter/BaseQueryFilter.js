class BaseQueryFilter{
    
    constructor(body){
        this.type = body.type == undefined ? null: body.type;
        this.y = body.y == undefined ? null: body.y;
        this.r = body.r == undefined ? "json": body.r;
        this.callback = body.callback == undefined ? null: body.callback;
        this.v = body.v == undefined ? null: body.v
    }
}

module.exports = BaseQueryFilter;