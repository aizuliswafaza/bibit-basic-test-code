-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bibit_task
CREATE DATABASE IF NOT EXISTS `bibit_task` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bibit_task`;

-- Dumping structure for table bibit_task.mst_logger
CREATE TABLE IF NOT EXISTS `mst_logger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `executedAt` datetime NOT NULL DEFAULT current_timestamp(),
  `method` varchar(50) DEFAULT NULL,
  `endpoint` varchar(255) NOT NULL DEFAULT '-',
  `queryString` text DEFAULT NULL,
  `payload` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table bibit_task.mst_logger: ~32 rows (approximately)
/*!40000 ALTER TABLE `mst_logger` DISABLE KEYS */;
INSERT INTO `mst_logger` (`id`, `executedAt`, `method`, `endpoint`, `queryString`, `payload`) VALUES
	(1, '2021-03-25 08:44:42', NULL, '/index', NULL, NULL),
	(2, '2021-03-25 02:19:52', NULL, '/', '{"plot":"full"}', '{}'),
	(3, '2021-03-25 02:20:50', NULL, '/api/v1/logs?plot=full', '{"plot":"full"}', '{}'),
	(4, '2021-03-25 02:22:18', NULL, '/api/v1/logs/', 'plot=full', '{}'),
	(5, '2021-03-25 02:23:08', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(6, '2021-03-25 02:39:46', 'GET', '/api/v1/movies/search', '?s=batman&plot=full', '{}'),
	(7, '2021-03-25 02:41:49', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(8, '2021-03-25 02:44:00', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(9, '2021-03-25 02:44:39', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(10, '2021-03-25 02:45:16', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(11, '2021-03-25 02:49:35', 'POST', '/api/v1/logs/', '?plot=full', '{}'),
	(12, '2021-03-25 03:06:39', 'GET', '/api/v2/movies/search', '?s=batman&plot=full', '{}'),
	(13, '2021-03-25 03:08:14', 'GET', '/api/v2/movies/detail', '?i=tt0372784&plot=full', '{}'),
	(14, '2021-03-25 03:09:05', 'GET', '/api/v2/movies/detail/tt0078346', '?plot=full', '{}'),
	(15, '2021-03-25 11:57:37', 'GET', '/api/v1/movies/search', '?plot=full', '{}'),
	(16, '2021-03-25 12:21:52', 'GET', '/api/v1/movies/search', '?type=&y=&r=json&callback=&v=&s=batman&page=1&plot=full', '{}'),
	(17, '2021-03-25 12:27:59', 'GET', '/api/v1/movies/detail', '?type=&y=&r=json&callback=&v=&i=tt0372784&i=tt0372784&t=&plot=short&plot=full', '{}'),
	(18, '2021-03-25 12:28:09', 'GET', '/api/v1/movies/detail', '?type=&y=&r=json&callback=&v=&i=tt0372784&i=tt0372784&t=&plot=full&plot=full', '{}'),
	(19, '2021-03-25 12:28:24', 'GET', '/api/v1/movies/detail', '?i=tt0372784&plot=full', '{}'),
	(20, '2021-03-25 12:28:41', 'GET', '/api/v1/movies/detail', '?%3Ftype=&y=&r=json&callback=&v=&i=tt0372784&i=tt0372784&t=&plot=short&plot=full', '{}'),
	(21, '2021-03-25 12:28:48', 'GET', '/api/v1/movies/detail', '?%3Ftype=&y=&r=json&callback=&v=&i=tt0372784&t=&plot=short', '{}'),
	(22, '2021-03-25 12:36:11', 'GET', '/api/v1/movies/search', '?s=batman&plot=full', '{}'),
	(23, '2021-03-25 12:36:36', 'GET', '/api/v1/movies/search', '?s=batman&plot=full', '{}'),
	(24, '2021-03-25 12:39:55', 'GET', '/api/v1/movies/search', '?s=batman&plot=full', '{}'),
	(25, '2021-03-25 12:49:12', 'GET', '/api/v1/movies/search', '?s=batman', '{}'),
	(26, '2021-03-25 12:49:39', 'GET', '/api/v1/movies/search', '?s=batman', '{}'),
	(27, '2021-03-25 12:52:52', 'GET', '/api/v1/movies/search', '?s=batman', '{}'),
	(28, '2021-03-25 12:52:52', 'GET', '/api/v1/movies/detail', '?i=tt0372784&plot=full', '{}'),
	(29, '2021-03-25 12:54:13', 'GET', '/api/v1/movies/search', '?s=batman', '{}'),
	(30, '2021-03-25 12:54:13', 'GET', '/api/v1/movies/detail', '?i=tt0372784&plot=full', '{}'),
	(31, '2021-03-25 12:55:19', 'GET', '/api/v1/movies/search', '?s=batman', '{}'),
	(32, '2021-03-25 12:55:20', 'GET', '/api/v1/movies/detail', '?i=tt0372784&plot=full', '{}');
/*!40000 ALTER TABLE `mst_logger` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
