var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/indexRoutes');
var logsRouter = require('./routes/logsRoutes');
var usersRouter = require('./routes/users');
var moviesRouter = require('./routes/moviesRoutes');
var moviesRouterV2 = require('./routes/moviesRoutesV2');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/v1', indexRouter);
app.use('/api/v1/logs', logsRouter);

//method log embedded in function
app.use('/api/v1/movies', moviesRouter);
//using middleware
app.use('/api/v2/movies', moviesRouterV2);

module.exports = app;
